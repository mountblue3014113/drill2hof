// 4. Find the sum of all salaries.
const sumOfAllSalaries = (data) => {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  const sumOfSalaries = data.reduce((sum, person) => {
    const salaryInNum = parseFloat(person.salary.replace("$", ""));

    if (!isNaN(salaryInNum)) {
      return sum + salaryInNum;
    } else {
      console.log("Not able to convert salary to pure num !");
    }
  }, 0);
  return sumOfSalaries;
};
module.exports = sumOfAllSalaries;
