// 1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

const findDevelopers = (data) => {
    if (!Array.isArray(data)) {
      return (`Data must be an Array !`);
    }
    if (data.length === 0) {
      return (`Data is empty !`);
    }
    const developers = data.filter((employee)=> employee.job.includes("Web Developer"));
    if (developers.length === 0) {
      return ("No one is Developer in the data set !");
    } else {
      return developers;
    }
  }
  module.exports = findDevelopers;
  