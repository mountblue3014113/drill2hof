const data = require("../data.js");
const sumOfAllSalaries = require("../problem4.js");

try {
  console.log(`The sum of all salaries is : $`+sumOfAllSalaries(data));
} catch (error) {
  console.log("Something went wrong");
}
