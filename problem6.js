// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
const convertSalaryValuesInToNumber = require("./problem2.js");

const avgSalariesByCountry = (data) => {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }
  data = convertSalaryValuesInToNumber(data);
  const averageSalaryOfCountry = data.reduce((averageSalaryOfCountry, person) => {
      if (!averageSalaryOfCountry[person.location]) {
        averageSalaryOfCountry[person.location] = averageSalaryOfCountry[person.location] || { sum: person.salary, count: 1 };
      } else {
        averageSalaryOfCountry[person.location].sum += person.salary;
        averageSalaryOfCountry[person.location].count += 1;
      }
      return averageSalaryOfCountry;
    },
    {}
  );

  for (index in averageSalaryOfCountry) {
    averageSalaryOfCountry[index] =
      averageSalaryOfCountry[index].sum / averageSalaryOfCountry[index].count;
  }
  return averageSalaryOfCountry;
};
module.exports = avgSalariesByCountry;
