// 2. Convert all the salary values into proper numbers instead of strings.
const convertSalaryValuesInToNumber = (data) => {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  const salaryInNum = data.map((person) => {
    const salaryNum = parseFloat(person.salary.replace("$", ""));
    if (!isNaN(salaryNum)) {
      person.salary = salaryNum;
      return person;
    } else {
      console.log("Not able to convert salary to pure num !");
    }
  });

  return salaryInNum;
};
module.exports = convertSalaryValuesInToNumber;
