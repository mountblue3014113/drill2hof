//  5. Find the sum of all salaries based on country. ( Group it based 
// on country and then find the sum ).
const sumOfSalariesByCountry = (data) =>{
    if (!Array.isArray(data)) {
      return `Data must be an Array !`;
    }
    if (data.length === 0) {
      return `Data is empty !`;
    }
  
    const salarySumByCountry = data.reduce((acc,person) => {
        const salaryInNum = parseFloat(person.salary.replace('$', ''));
        acc[person.location]=(acc[person.location] || 0 )+salaryInNum;
        return acc;
    }, {});
    return salarySumByCountry;
  }
  module.exports = sumOfSalariesByCountry;