// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const salaryAfterCorrection = (data) => {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  const correctedSalary = data.map((person) => {
    const salaryNum = parseFloat(person.salary.replace("$", ""));

    if (!isNaN(salaryNum)) {
      person.corrected_salary = salaryNum * 10000;
      return person;
    } else {
      console.log("Not able to convert salary to pure num !");
    }
  });
  return correctedSalary;
};
module.exports = salaryAfterCorrection;
